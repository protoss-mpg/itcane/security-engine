# security-engine

Security Service Engine
Run Test
$mvn clean compile test -Dspring.profiles.active=test

Start Project
$mvn spring-boot:run

#Step Sonar
mvn clean install sonar:sonar -Dspring.profiles.active=test sonar:sonar -Dsonar.projectKey=securityEngine -Dsonar.host.url=http://localhost:9000
