package com.mpg.itcane.security;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.transaction.Transactional;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class SecurityEngineApplicationTests {

	@Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;
    

    @Before
    public void setUp()   {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
         
    }

    @After
    public void tearDown(){
    }
    
    @Test
	public void testFormSubmitAuthentication() throws Exception {
    	log.info("====================== Start Test");
        String response = mockMvc.perform(post("/login")
        		.param("username", "teetiratv")
        		.param("password", "Mercury4420"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        log.info("====================== response={}",response);
        
    }
    
//    @Test
//	public void testBasicAuthentication() throws Exception {
//    	log.info("====================== Start Test");
//        String response = mockMvc.perform(post("/authenticate")//.header(name, values)
//        		.param("username", "teetiratv")
//        		.param("password", "Mercury4420"))
//                .andExpect(status().isOk())
//                .andReturn().getResponse().getContentAsString();
//        log.info("====================== response={}",response);
//        
//    }

}
