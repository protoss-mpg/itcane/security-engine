INSERT INTO app_role(id,version,create_by, create_date,update_by, update_date,flag_active,role_name)
	VALUES(100,0,'Admin', null,null, null,1,'USER');
INSERT INTO app_role(id,version,create_by, create_date,update_by, update_date,flag_active,role_name)
	VALUES(101,0,'Admin', null,null, null,1,'ADMIN');
INSERT INTO app_role(id,version,create_by, create_date,update_by, update_date,flag_active,role_name)
	VALUES(102,0,'Admin', null,null, null,1,'FARMER');
INSERT INTO app_role(id,version,create_by, create_date,update_by, update_date,flag_active,role_name)
	VALUES(103,0,'Admin', null,null, null,1,'VENDOR');

	
INSERT INTO app_menu (id,create_by,create_date,update_by,update_date,version,flag_active,name_lang2,name_lang3,name_lang4,name_lang5,name_lang6,name_lang7,name_lang8,
						code ,name_lang1,program_path,url_link,parent,sequence,flag_has_child,icon)
	VALUES(300,'Admin',null,null,null,0,1,null,null,null,null,null,null,null, 
		   'R001' ,'Dashboard',null,'/dashboard',null,1,0,'icon-speedometer');
INSERT INTO app_menu (id,create_by,create_date,update_by,update_date,version,flag_active,name_lang2,name_lang3,name_lang4,name_lang5,name_lang6,name_lang7,name_lang8,
						code ,name_lang1,program_path,url_link,parent,sequence,flag_has_child,icon)
	VALUES(302,'Admin',null,null,null,0,1,null,null,null,null,null,null,null, 
		   'R002' ,'Theme',null,null,null,2,0,null);
INSERT INTO app_menu (id,create_by,create_date,update_by,update_date,version,flag_active,name_lang2,name_lang3,name_lang4,name_lang5,name_lang6,name_lang7,name_lang8,
						code ,name_lang1,program_path,url_link,parent,sequence,flag_has_child,icon)
	VALUES(303,'Admin',null,null,null,0,1,null,null,null,null,null,null,null, 
		   'R003' ,null,null,null,null,3,0,null);
INSERT INTO app_menu (id,create_by,create_date,update_by,update_date,version,flag_active,name_lang2,name_lang3,name_lang4,name_lang5,name_lang6,name_lang7,name_lang8,
						code ,name_lang1,program_path,url_link,parent,sequence,flag_has_child,icon)
	VALUES(304,'Admin',null,null,null,0,1,null,null,null,null,null,null,null, 
		   'R004' ,'Base',null,'/base',null,1,1,'icon-puzzle');
INSERT INTO app_menu (id,create_by,create_date,update_by,update_date,version,flag_active,name_lang2,name_lang3,name_lang4,name_lang5,name_lang6,name_lang7,name_lang8,
						code ,name_lang1,program_path,url_link,parent,sequence,flag_has_child,icon)
	VALUES(3041,'Admin',null,null,null,0,1,null,null,null,null,null,null,null, 
		   'R0041' ,'Cards',null,'/base/cards',304,1,0,'icon-puzzle');
INSERT INTO app_menu (id,create_by,create_date,update_by,update_date,version,flag_active,name_lang2,name_lang3,name_lang4,name_lang5,name_lang6,name_lang7,name_lang8,
						code ,name_lang1,program_path,url_link,parent,sequence,flag_has_child,icon)
	VALUES(3042,'Admin',null,null,null,0,1,null,null,null,null,null,null,null, 
		   'R0042' ,'Carousels',null,'/base/carousels',304,1,0,'icon-puzzle');
						
INSERT INTO menu_role ( menu_id,role_id)
	VALUES(300,100);	
INSERT INTO menu_role ( menu_id,role_id)
	VALUES(302,100);	
INSERT INTO menu_role ( menu_id,role_id)
	VALUES(303,100);	
INSERT INTO menu_role ( menu_id,role_id)
	VALUES(304,100);	
INSERT INTO menu_role ( menu_id,role_id)
	VALUES(3041,100);		
INSERT INTO menu_role ( menu_id,role_id)
	VALUES(3042,100);		
