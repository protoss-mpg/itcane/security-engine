CREATE TABLE IF NOT EXISTS  app_menu (
	id bigint not null, 
	create_by varchar(255), 
	create_date timestamp, 
	update_by varchar(255), 
	update_date timestamp, 
	version bigint, 
	flag_active bit NULL, 
	code varchar(255), 
	name_lang1 varchar(255), 
	name_lang2 varchar(255), 
	name_lang3 varchar(255), 
	name_lang4 varchar(255), 
	name_lang5 varchar(255), 
	name_lang6 varchar(255), 
	name_lang7 varchar(255), 
	name_lang8 varchar(255), 
	program_path varchar(255), 
	url_link varchar(255), 
	parent bigint, 
	sequence integer, 
	flag_has_child boolean, 
	icon varchar(255), 
	primary key (id));
	
CREATE TABLE IF NOT EXISTS  app_role (
	id bigint not null, 
	create_by varchar(255), 
	create_date timestamp, 
	flag_active bit NULL, 
	role_name varchar(255), 
	update_by varchar(255), 
	update_date timestamp, 
	version bigint, 
	primary key (id));
	
CREATE TABLE IF NOT EXISTS  app_user (
	id bigint not null, 
	access_token varchar(255), 
	account_non_expired boolean, 
	account_non_locked boolean, 
	create_by varchar(255), 
	create_date timestamp, 
	credentials_non_expired boolean, 
	description varchar(255), 
	email varchar(255), 
	first_name varchar(255), 
	last_name varchar(255), 
	update_by varchar(255), 
	update_date timestamp, 
	username varchar(255), 
	version bigint, 
	primary key (id));
	
CREATE TABLE IF NOT EXISTS  menu_role (
	menu_id bigint not null, 
	role_id bigint not null, 
	primary key (menu_id, role_id));
	
CREATE TABLE IF NOT EXISTS  user_role (
	user_id bigint not null, 
	role_id bigint not null, 
	primary key (user_id, role_id));
