package com.mpg.itcane.security.controller;

import java.util.Base64;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mpg.itcane.security.model.ResponseModel;
import com.mpg.itcane.security.service.SecurityService;
import com.protosstechnology.commons.util.ResponseUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class SecurityController {

     @Autowired
     private SecurityService securityService;
     
     public  String[] getUsernamePassword(HttpServletRequest request){
 		if(request.getHeader("authorization") !=null){
 			String userToken = request.getHeader("authorization").replaceAll("Basic ", "");
 			byte[] decodedBytes = Base64.getDecoder().decode(userToken);
 			String decodedString = new String(decodedBytes);
 			return decodedString.split(":");
 		}else {
 			return null;
 		}
 		
 	}

     @PostMapping("/login")
     public ResponseEntity<ResponseModel> login(@RequestParam("username") String username,@RequestParam("password") String password) {
              log.info("=====================Security Login ======");
              log.info("=====================username={}",username);
         	 return ResponseUtil.getResponseOk().body(securityService.authenticationProcess(username, password));
     }


     
     @PostMapping("/authenticate")
     public ResponseEntity<ResponseModel> authenticate(HttpServletRequest request) {
    	 log.info("=====================Security Authenticate ======");
         String[] dataAuthen = getUsernamePassword(request);
         String username = dataAuthen[0];
         String password = dataAuthen[1];
         log.info("=====================username={}",username);
    	 return ResponseUtil.getResponseOk().body(securityService.authenticationProcess(username, password));
     }


}

