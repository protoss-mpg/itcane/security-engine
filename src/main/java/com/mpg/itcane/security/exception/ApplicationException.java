package com.mpg.itcane.security.exception;

public class ApplicationException extends RuntimeException {

    public ApplicationException(){

    }

    public ApplicationException(String errorMessage) {
        super(errorMessage);

    }
}
