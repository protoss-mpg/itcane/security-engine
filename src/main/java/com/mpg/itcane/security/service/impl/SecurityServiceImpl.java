package com.mpg.itcane.security.service.impl;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.mpg.itcane.security.entity.Menu;
import com.mpg.itcane.security.entity.Role;
import com.mpg.itcane.security.entity.User;
import com.mpg.itcane.security.exception.ApplicationException;
import com.mpg.itcane.security.model.ResponseModel;
import com.mpg.itcane.security.model.UserModel;
import com.mpg.itcane.security.repository.MenuRepository;
import com.mpg.itcane.security.repository.RoleRepository;
import com.mpg.itcane.security.repository.UserRepository;
import com.mpg.itcane.security.service.SecurityService;
import com.protosstechnology.commons.util.JSONUtil;
import com.protosstechnology.commons.util.RequestUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SecurityServiceImpl implements SecurityService {
	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	MenuRepository menuRepository;
	
	@Autowired
	RoleRepository roleRepository;
	
	@Value("${authentication.ad.api.key}")
	private String authenticationAdApiKey;
	
	@Value("${authentication.ad.server}")
	private String authenticationAdServer;
	
	@Value("${authentication.farm.server}")
	private String authenticationFarmServer;
	
	@Value("${authentication.vendor.server}")
	private String authenticationVendorServer;
	
	public ResponseModel getAuthorities(User user,Map<String,String> userServiceMap) {
		ResponseModel response = new ResponseModel();
		
		try {
			
			UserModel userModel = new UserModel();
			userModel.setUserName(user.getUsername());
			userModel.setEmployeeId(userServiceMap.get("employeeId"));
			userModel.setFarmerId(userServiceMap.get("farmerId"));
			userModel.setVendorId(userServiceMap.get("vendorId"));
			
			String roleIdLsStr = "";
			String roleNameLsStr = "";
			List<Long> roleIdLs = new ArrayList();
			
			
			if(user!=null) {
				/* Get Role */
				Set<Role> authorities = user.getAuthorities();
				if(authorities!=null) {
					userModel.setAuthoritiesEntity(authorities);
					
					for(Role authority: user.getAuthorities()) {
						roleIdLs.add(authority.getId());
						roleIdLsStr = roleIdLsStr+","+authority.getId();
						roleNameLsStr = roleNameLsStr + "," + authority.getRole();
					}
				}
				
				log.info("====================roleIdLs={}",roleIdLs);
				log.info("====================roleNameLsStr={}",roleNameLsStr);
				
				/* Get Menu */
				Pageable sortedByParentAndSequence =  PageRequest.of(0, 10000, Sort.by("parent").and(Sort.by("sequence")));
				Page<Menu> menuLs = menuRepository.findByCodeIgnoreCaseContainingAndNameIgnoreCaseContainingAndRolesIn("", "", roleIdLs, sortedByParentAndSequence);
				
				
				if(!roleIdLs.isEmpty()) {
					userModel.setMenusEntity(menuLs.getContent());
				}
				
				
				response.setSuccess(true);
				response.setMessage("Authentication Successful.");
				response.setData(userModel);
			}
			
			
		} catch (Exception e) {
			log.error("Error={}",e);
			response.setSuccess(false);
			response.setMessage(e.getMessage());
		}
		
		return response;
	}
	
	private boolean authenticationDB(String username,String password) {
		boolean retResult = false;
		User user = userRepository.findByUsername(username);
		if(user != null && user.getAccessToken()!=null) {
			retResult = DigestUtils.md5Hex(password).equals(user.getAccessToken());
		}
		
		return retResult;
	}
	
	private boolean authenticationActiveDirectory(String username,String password,Map userServiceMap) {
		boolean retResult = false;
		String urlParam = authenticationAdServer+"/authentication/login";
		Map<String, String> parameterMap = new HashMap();
		parameterMap.put("username", username);
		parameterMap.put("password", password);
		String jsonBody = postAuthenActiveDirectory(parameterMap,urlParam).getBody();
		String userId = JSONUtil.fromJsonPath(jsonBody, String.class, "success.data.user_info.id");
		
		if(userId!=null) {
			userServiceMap.put("employeeId", userId);
			retResult = true;
		}
		
		return retResult;
	}
	
	private boolean authenticationFarm(String username,String password,Map userServiceMap) {
		boolean retResult = false;
		String urlParam = authenticationFarmServer+"/iCane/BusinessPartner/X3PVB/GetBPForGIS/IDcard/"+username;
		String jsonBody = restTemplate.getForEntity(urlParam, String.class).getBody();
		List<Map<String,Object>> results = JSONUtil.fromJsonPath(jsonBody, List.class, "results");
		for(Map<String,Object> quotaData :results) {
			if(password.equals(quotaData.get("quota_no"))) {
				userServiceMap.put("farmerId", quotaData.get("id_no"));
				retResult = true;
				break;
			}
		}
		
		return retResult;
	}
	
	private boolean authenticationVendor(String username,String password,Map userServiceMap) {
		boolean retResult = false;
		String urlParam = authenticationVendorServer+"/iCane/contractor/X3PVB/info/IDcard/"+username;
		String jsonBody = restTemplate.getForEntity(urlParam, String.class).getBody();
		List<Map<String,Object>> results = JSONUtil.fromJsonPath(jsonBody, List.class, "results");
		for(Map<String,Object> quotaData :results) {
			if(password.equals(quotaData.get("bp_code"))) {
				userServiceMap.put("vendorId", quotaData.get("id_no"));
				retResult = true;
				break;
			}
		}
		
		return retResult;
	}
	
	
	@Override
	public ResponseModel authenticationProcess(String username,String password) {
		ResponseModel response = new ResponseModel();
		
		try {
			
			Map userServiceMap = new HashMap();
			User user = userRepository.findByUsername(username);
			Set<Role> roleSet = new HashSet();
			
			boolean isPassFARMAuthen  = authenticationFarm(username,password,userServiceMap);
			boolean isPassVendorAuthen= authenticationVendor(username,password,userServiceMap);
			boolean isPassADAuthen    = authenticationActiveDirectory(username,password,userServiceMap);
			
			if(isPassFARMAuthen ){
				roleSet.add(roleRepository.findByRoleName("FARMER"));
			}
			
			if(isPassVendorAuthen ){
				roleSet.add(roleRepository.findByRoleName("VENDOR"));
			}
			
			if(isPassADAuthen ){
				roleSet.add(roleRepository.findByRoleName("USER"));
			}
			
			if( !isPassVendorAuthen && !isPassFARMAuthen && !isPassADAuthen && !authenticationDB(username,password) ) {
				throw new ApplicationException("Authentication Fail.");
			}
			
			if((isPassVendorAuthen || isPassFARMAuthen || isPassADAuthen) &&  user == null) {
				//Create User
				user = new User();
				user.setUsername(username);
				user.setAccountNonExpired(true);
				user.setAccountNonLocked(true);
				user.setCreateBy("SYSTEM");
				user.setCredentialsNonExpired(true);
				user.setRoles(roleSet);
				
				userRepository.saveAndFlush(user);
			}
			
			if( user != null) {
				return getAuthorities(user,userServiceMap);
			}
			
		} catch (Exception e) {
			log.error("Error={}",e);
			response.setSuccess(false);
			response.setMessage(e.getMessage());
		}
		
		
		return response;
	}
	
	
	private  ResponseEntity<String> postAuthenActiveDirectory(Map<String, String> parameterMap, String urlParam) {
		String url = urlParam;
	    log.info("========================== url :{}", url);
	
	    MediaType mediaType = new MediaType("application", "x-www-form-urlencoded", StandardCharsets.UTF_8);
	    HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(mediaType);
	    headers.add("App-Key", this.authenticationAdApiKey);
	
	    
	    MultiValueMap<String, String> body = RequestUtil.getMultiValueMap(parameterMap);
	
	
	    HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity(body, headers);
	
	    FormHttpMessageConverter converter = new FormHttpMessageConverter();
	    converter.setSupportedMediaTypes(Arrays.asList(mediaType));
	
	    restTemplate.getMessageConverters().add(converter);
	    return restTemplate.postForEntity(url, entity, String.class);
	    
	}



}