package com.mpg.itcane.security.service;

import com.mpg.itcane.security.model.ResponseModel;

public interface SecurityService {

	public ResponseModel authenticationProcess(String username,String password);
}