package com.mpg.itcane.security.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
@Table(name="app_user")
public class User {
	
	private @Id
    @GeneratedValue(strategy= GenerationType.TABLE)Long id;
	private @Version
    @JsonIgnore
    Long version;
	private String createBy;
	private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createDate;
	private String updateBy;
	private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
	@Column(unique = true)
	@NonNull
    private String username;
	@NonNull
    private String accessToken;
	@NonNull
    private String firstName;
	@NonNull
    private String lastName;
	private @Formula("CONCAT(first_name,' ',last_name)") String fullName;
	private String description;
	private Boolean accountNonExpired;
	private Boolean credentialsNonExpired;
	private Boolean accountNonLocked;
	private String  email;
	
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "userRole", joinColumns = {
			@JoinColumn(name = "user_id", nullable = false, updatable = false) },
			inverseJoinColumns = { @JoinColumn(name = "role_id",nullable = false, updatable = false) })
    private Set<Role> roles = new HashSet<Role>();
	
	public Set<Role> getAuthorities(){
		return this.roles;
	}
	
}
