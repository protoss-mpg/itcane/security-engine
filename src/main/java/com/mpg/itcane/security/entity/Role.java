package com.mpg.itcane.security.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
@Table(name="app_role")
public class Role {
	
	
	private @Id
    @GeneratedValue(strategy= GenerationType.TABLE)Long id;
	private @Version
    @JsonIgnore
    Long version;
	private String createBy;
	private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp createDate;
	private String updateBy;
	private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;
	
	
	private @Formula("CONCAT('ROLE_',role_name)") String role;
	private @Column(unique=true) String roleName;
	private Boolean flagActive;
	
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "roles")
    private Set<User> user = new HashSet<User>();
	

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "roles")
    private Set<Menu> menu = new HashSet<Menu>();
	
}
