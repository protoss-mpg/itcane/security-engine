package com.mpg.itcane.security.model;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
public class MenuModel {

	@JsonIgnore private Long id;
	@JsonInclude(Include.NON_NULL) private String name;
	@JsonInclude(Include.NON_NULL) private String url;
	@JsonInclude(Include.NON_NULL) private String icon;
	@JsonInclude(Include.NON_NULL) private Boolean title;
	@JsonInclude(Include.NON_NULL) private List children;
	@JsonInclude(Include.NON_NULL) private Map badge;
	@JsonInclude(Include.NON_NULL) private Boolean divider;
	
}
