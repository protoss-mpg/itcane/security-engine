package com.mpg.itcane.security.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.mpg.itcane.security.entity.Menu;
import com.mpg.itcane.security.entity.Role;

import lombok.Data;

@Data
public class UserModel {

	private String employeeId;
	private String farmerId;
	private String vendorId;
	private String userName;
	private List<AuthorityModel> authorities;
	private List<MenuModel> menus;
	
	public void setAuthoritiesEntity(Set<Role> authorities) {
		List<AuthorityModel> authoritiesTemp = new ArrayList();
		AuthorityModel model = null;
		for(Role authoritie:authorities) {
			model = new AuthorityModel();
			model.setRoleId(authoritie.getId());
			model.setRoleName(authoritie.getRole());
			authoritiesTemp.add(model);
		}
		this.setAuthorities(authoritiesTemp);
	}
	
	public void setMenusEntity(List<Menu> menus) {
		List<MenuModel> menusTemp = new ArrayList();
		MenuModel model = null;
		
		Map<Long,Object> childLsMap = new HashMap();
		for(Menu menu:menus) {
			List<MenuModel>  childLs = (List<MenuModel>) childLsMap.get(menu.getParent());
			if(menu.getParent()!=null && childLs==null) {
				childLs = new ArrayList();
			}
			
			model = new MenuModel();
			model.setId(menu.getId());
			if(!menu.getName().trim().isEmpty()) {
				model.setName(menu.getName().trim());
				if(menu.getUrlLink() !=null) {
					model.setUrl(menu.getUrlLink());
					model.setIcon(menu.getIcon());
				}else {
					model.setTitle(true);
				}
			}else {
				model.setDivider(true);
			}
			
			if(menu.getParent()!=null) {
				childLs.add(model);
				childLsMap.put(menu.getParent(), childLs);
			}else {
				menusTemp.add(model);
			}
			
		}
		
		//get Map<Parent,List<Child>> put to child
		for(MenuModel menuModel:menusTemp) {
			List<MenuModel>  childLs = (List<MenuModel>) childLsMap.get(menuModel.getId());
			if(childLs!=null && !childLs.isEmpty()) {
				menuModel.setChildren(childLs);
			}
			
		}
		
		this.setMenus(menusTemp);
	}
}
