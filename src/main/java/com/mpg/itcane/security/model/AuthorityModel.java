package com.mpg.itcane.security.model;

import lombok.Data;

@Data
public class AuthorityModel {

	private Long roleId;
	private String roleName;
	
}
